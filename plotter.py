#!/usr/bin/env python

'''
    Haplostrips

    Copyright 2016 Davide Marnetto

    This file is part of Haplostrips.

    Haplostrips is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Haplostrips is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

'''

from __future__ import with_statement
#from sys import stdin, stderr
from optparse import OptionParser
from subprocess import Popen 
from os import walk, path, remove
from sys import stderr
from shutil import move
import platform

def R_path(mypath):
	hconfig=open("/".join([mypath,"haplostrips.config"]),'r')
	for line in hconfig:
		if "R_PATH" in line:
			R_path=line[7:].rstrip()
			break
	if platform.system() == 'Windows' and R_path=="":
		for root, dirs, files in walk('C:/'):
			if 'Rscript.exe' in files:
				R_path=path.join(root, 'Rscript.exe')
				break
		if R_path=="":
			raise EnvironmentError('no Rscript.exe found in C:/ but needed, please install R or specify a path in haplostrips.config where to find it')
		hconfig.seek(0)
		with open("/".join([mypath,"haplostrips.config.tmp"]),'w') as new_hconfig:
			for line in hconfig:
				if "R_PATH" in line:
					new_hconfig.write("R_PATH="+R_path+"\n")
				else:
					new_hconfig.write(line)
		hconfig.close()
		#Remove original file
    		remove("/".join([mypath,"haplostrips.config"]))
		#Move new file
		move("/".join([mypath,"haplostrips.config.tmp"]), "/".join([mypath,"haplostrips.config"]))
	else:
		hconfig.close()
	return R_path if R_path!="" else "Rscript"
	

#need to add refpop and outpop
def plotter(haps_in,
	outpdf,
	add_poptable,
	interval,
	populations,
	private_cutoff,
	hapsort,
	ref,
	colors,
	tables,
	tree,
        missing,
	debug,
	colderived):

	if not path.isfile(haps_in):
		raise IOError('needed file ('+haps_in+') does NOT exist.')

	if interval:
		interval=interval[0]+"-"+str(interval[1])+"-"+str(interval[2])	

	roptions={'add_poptable': add_poptable,
		'interval'	: interval,
		'spops'		: populations,
		'private_cutoff': private_cutoff,
		'hapsorder'	: hapsort,
		'ref'		: ref,
		'colors'	: colors,
		'tables'	: tables,
		'tree'		: tree,
                'missing'       : missing,
		'colderived'	: colderived
		}
	to_del=[]
	for r in roptions:
		if roptions[r] != None:
			roptions[r]=r+"=\'"+str(roptions[r])+"\'"
		else:
			to_del.append(r)

	for r in to_del:
		del roptions[r]

	if debug:
		roptions['debug']='debug=T'

	mypath=path.dirname(path.realpath(__file__))
	if platform.system() == 'Windows':
		mypath_new=mypath.replace("\\","/")
		mypath=mypath_new
		haps_in_new=haps_in.replace("\\","/")
		haps_in=haps_in_new
		outpdf_new=outpdf.replace("\\","/")
		outpdf=outpdf_new
		if 'add_poptable' in roptions:
			add_poptable_new=roptions['add_poptable'].replace("\\","/")
			roptions['add_poptable']=add_poptable_new
	rscript="source(\'"+mypath+"/plotter_general.r\'); plot_haplotypes(\'"+haps_in+"\',\'"+outpdf+"\',\'"+mypath+"/default.poptable\',"+",".join([roptions[r] for r in roptions])+")"
	rscript_exec=[R_path(mypath), "--vanilla", "-e", rscript]
	if debug:
		print " ".join(rscript_exec)
	r=Popen(rscript_exec)
        r.communicate()


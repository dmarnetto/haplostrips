#!/usr/bin/env python 

'''
    Haplostrips

    Copyright 2016 Davide Marnetto

    This file is part of Haplostrips.

    Haplostrips is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Haplostrips is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

'''

#from sys import stdin, stderr
#from subprocess import call
from __future__ import with_statement
from optparse import OptionParser, OptionGroup
import haplotyper
import plotter
import tempfile
import sys
#from os import environ

# define the name prefix of the outfile
def get_outname(interval,anc_allele):
	if anc_allele:
		anc='ANC'
	else:
		anc='REF'
	outfile="vcf_in-"+"_".join(str(i) for i in interval)+"-"+anc
	return outfile


def do_haplostrips(ho,po,mo): #haplotyper, plotter and main options
		if ho['interval']:
			intraw1,intraw2 = ho['interval'].split(':')
			intraw3,intraw4 = intraw2.split('-')
			ho['interval']=(intraw1,int(intraw3),int(intraw4))

		if ho['vcf_in']:
			if not mo['outprefix']:
				outhaps=get_outname(ho['interval'],ho['anc_allele'])+".haplostrips.haps"
			else:
				outhaps=mo['outprefix']+".haps"
				
			haplotyper.haplotyper(outhaps=outhaps, debug=mo['debug'], **ho)

			mo['haps_in']=outhaps
			ho['interval']=None
		elif mo['ms_in']:
			poptempfile=tempfile.NamedTemporaryFile()
			po['add_poptable']=poptempfile.name
			if not mo['outprefix']:
				outhaps="ms_in.haplostrips.haps"
			else:
				outhaps=mo['outprefix']+".haps"

			haplotyper.haps_from_ms(mo['ms_in'],outhaps,po['add_poptable'])
			mo['haps_in']=outhaps

		if not mo['outprefix']:
			outpdf=mo['haps_in'][:-17]+"."+str(po['populations'])+".haplostrips.pdf"
		else:
			outpdf=mo['outprefix']+".pdf"

		sys.stdout.flush()
			
		plotter.plotter(haps_in=mo['haps_in'], outpdf=outpdf, interval=ho['interval'], debug=mo['debug'], **po)

def main():
	usage = '''
Haplostrips version 1.2.1

%prog  [OPTIONS]

Produces Haplostrips plots, that depict the variants in a window of genomic sequence for different samples. Visualizes the number of different haplotypes, similarities between each other and with respect to a reference haplotype through haplotype clustering and sorting, thus revealing hidden population haplotype structure.

One option among -v, -V, -H or -s is mandatory.
Choose the populations that you want to plot with -p, by default all populations are plotted(discouraged). Notice that the default palette supports up to 12 populations.
The interval must be supplied if the input is a vcf, with -i or -m options.
If a tabix (.tbi) index file is available, haplostrips will use the package pysam (http://pysam.readthedocs.io/en/latest/api.html) to retrieve your window faster.
Region to be plotted should NOT contain more than ~10K sites (e.g: less than 300kb if you are using 1000 genomes phase 3 vcfs).

For more information see https://bitbucket.org/dmarnetto/haplostrips/overview .

if -m / --multi: optional MULTIFILE contains the info for each plot you want, and it should contain 5 fields:
	1	chr
	2	start
	3	stop
	4	populations
	5	outfile prefix (. for not defined)
	...	other fields will not be used
	'''

	parser = OptionParser(usage=usage)
	group = OptionGroup(parser, "Input Options")

	group.add_option('-v', '--vcf_inner', dest='vcf_inner',default=[], action='append', help=' Input from a VCF file. If more vcfs are supplied and variations are not present in other vcfs, they are simply skipped', metavar='FILE')
	group.add_option('-V', '--vcf_full', dest='vcf_full',default=[], action='append', help='Input from a VCF file. If more vcfs are supplied and variations are not present in other vcfs, sets them to missing. (to recover the old behaviour use -R -V FILE)', metavar='FILE')
	group.add_option('-R', '--missing2ref', dest='missing2ref', action='store_true', default=False, help='If more vcfs are supplied and variations are not present in some vcfs, it assumes that all variations missing in vcfs supplied with -v are homozygous for the reference allele found in files supplied with -V (old default behaviour)')
	group.add_option('-H', '--haps', dest='haps_in', default=None, help='input from haps file FILE, if -m / --multi is specified, \"-H multi\" causes the program to look for *.haps files in the directory, where * is the prefix given in the MULTIFILE or the default prefix', metavar='FILE|\"multi\"')
	group.add_option('-s', '--ms_in', dest='ms_in', default=None, help='input from ms simulation output FILE', metavar='FILE')
	group.add_option('-i', '--interval', dest='interval', default=None, type=str, help='interval of which you want to draw the plot, written as CHR:START-STOP. CHR must match exactly a chromosome name in your input, be careful with notations (e.g. chr1:START-STOP vs 1:START:STOP) ', metavar='CHR:START-STOP')
	group.add_option('-m', '--multi', dest='multi', default=None, help='you can supply a MULTIFILE to create a plot for each row, with the info contained in the multifile. incompatible with -i,-p,-F,-o', metavar='FILE')
	group.add_option('-P', '--poptable', dest='add_poptable', default=None, help='file containing samples and their populations. Individuals are on rows, 2 or 3 columns accepted: ID in the vcf, population, (optional) superpopulation. Populations metadata for DenisovaPinky, AltaiNean and the 1000 genomes phase 3 samples are already available by default', metavar='FILE')
	parser.add_option_group(group)

	group = OptionGroup(parser, "Output Options")
	group.add_option('-o', '--out', dest='outprefix', default=None, type=str, help='prefix of the output file, by default parse the arguments and give a meaningful name')
	group.add_option('-t','--tables', action="store_const",const="TRUE", dest='tables', default=None, help='create 2 tab delimited files: \"distances_tab\" contains the distances with respect to the reference haplotype for each haplotype plotted, \"mat\" contains the matrix that is plotted. All individuals are in the same order of the plot' )
	parser.add_option_group(group)
	
	group = OptionGroup(parser, "Variant Options")
	group.add_option('-a', '--anc_allele', dest='anc_allele', action='store_true', default=False, help='polarize sites when the ancestral allele is available and discards non-polarized sites\n(by default REF allele is used). Note that it takes the ancestral allele from the AA= flag of the vcf INFO field')
	group.add_option('-G','--GQ', type=float, dest='GQ', default=None, help='minimum genotype quality. If specified, and if the FORMAT field includes GQ, it is taken from the genotype fields of the vcf file, otherwise this filter is skipped. Non-number GQ fields are converted to -1. Values around 30-40 are advised', metavar='CUTOFF')
	group.add_option('-M','--MQ', type=float, dest='MQ', default=None, help='minimum mapping quality. Filtering performed in the same way as GQ (-G)', metavar='CUTOFF')
	parser.add_option_group(group)
	parser.add_option('-D', '--debug', dest='debug', action='store_true', default=False, help='debug option, set traceback limit to 5')

	group = OptionGroup(parser, "Plot Options")
	#PLOTTER OPTIONS
	group.add_option('-c','--min_priv_maf', type=float, dest='private_cutoff', default=0.05, help='remove sites with a private minor allele frequency under CUTOFF in all populations independently, defaults to 0.05', metavar='CUTOFF')
	group.add_option('-p', '--pops', dest='pops', default=None, type=str, help='Specify the populations to plot separated by commas: the first is the reference population (for sorting).the populations, with the exception of the reference population, can be specified as superpopulations.', metavar='REFPOP,OTHERPOPS')
	group.add_option('-r', '--ref', dest='ref', default=None, type=str, help='define an haplotype to consider reference: the value is a string that matches an haplotype name in the haps file. This option overrides the order given with -p/--pops.', metavar='HAPLOTYPE')
	group.add_option('-w', '--plot_missing', dest='missing', action='store_true', default=False, help='also plot missing data.')
	group.add_option('-d', '--colderived', dest='colderived', action='store_true', default=False, help='mark in blue the alternative/derived alleles in common with the reference haplotype')
	group.add_option('-S', '--hapsort', dest='hapsort', default=1, type=int, help='choose the sorting method for the haplotypes: 0 = no sorting; 1 = clustering and sorting for distance from the reference (default); 2 = sorting for distance from the reference; 3 = sorting for population and clustering.', metavar='0|1|2|3')
	group.add_option('-T', '--tree', action="store_const",const='row', dest='tree', default=None, help='draw a haplotype clustering tree (only compatible with sorting option -S 1).')
	group.add_option('-C', '--colors', dest='colors', default=None, type=str, help='define an alternative color palette in a color format accepted by R, eg. "red,blue,orange" or "#060633,#E6F0FF,#E41A1C. The order is the same as the populations supplied with -p."', metavar='COL1,COL2,...,COLN')
	parser.add_option_group(group)
	
        global options
	options, args = parser.parse_args()
	
	sys.tracebacklimit=0
	if options.debug:
		sys.tracebacklimit=5

	if len(args) != 0:
		raise IOError('argument found but no arguments expected. option -h for usage')

	vcf_in=[]
	for i in options.vcf_inner:
		vcf_in.append([0,i])
	for i in options.vcf_full:
		vcf_in.append([1,i])

	input_opts=0
	if vcf_in:
		input_opts+=1
		if not options.interval and not options.multi:
			raise IOError('interval (-i) must be specified. option -h for usage')
	if options.haps_in:
		input_opts+=1
	if options.ms_in:
		input_opts+=1
		if options.interval:
			raise IOError('interval (-i) not compatible with -s. option -h for usage')
	if input_opts!=1:
		raise IOError('exactly one among -v/-H/-s must be specified. option -h for usage')

	if options.private_cutoff > 0.5:
		raise IOError('minor allele frequency cannot be > 0.5. please correct -c value')
	
	mainopts={'outprefix':options.outprefix,
		'haps_in':options.haps_in,
		'ms_in':options.ms_in,
		'debug':options.debug
	}

	haplotyperopts={'vcf_in':vcf_in,
		'interval':options.interval,
		'anc_allele':options.anc_allele,
		'missing2ref':options.missing2ref,
		'GQ':options.GQ,
		'MQ':options.MQ
	}


	plotteropts={'populations':options.pops,
		'private_cutoff':options.private_cutoff,
		'add_poptable':options.add_poptable,
		'hapsort':options.hapsort,
		'ref':options.ref,
		'colors':options.colors,
		'tables':options.tables,
		'tree':options.tree,
		'missing':options.missing,
		'colderived':options.colderived
	}

        if not options.multi:
		do_haplostrips(haplotyperopts,plotteropts,mainopts)
	else: 	
		if options.interval or options.pops or options.outprefix or options.ms_in:
			raise IOError('-m not compatible with -i,-s,-p,-o. option -h for usage')

		with haplotyper.safe_open(options.multi, 'r') as fd:
			errors=[]
			for line in fd:
				f = line.rstrip().split('\t')
				if not len(f) >= 5:
					raise IOError('\"'+line.rstrip()+'\": at least 5 fields necessary in multi file')

				inter=f[0]+":"+f[1]+"-"+f[2]

				if options.haps_in:
					if options.haps_in == "multi" :
						haps_in=f[4]+".haps"
					else:
						raise IOError('in multi mode only "multi" value is accepted for -H option. option -h for usage')
				else:
					haps_in = None

				plotteropts['populations']=f[3]
				mainopts['haps_in']=haps_in
				mainopts['outprefix']=f[4]
				haplotyperopts['interval']=inter
				do_haplostrips(haplotyperopts,plotteropts,mainopts)

if __name__ == '__main__':
	main()


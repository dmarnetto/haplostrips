#!/usr/bin/env python

'''
    Haplostrips

    Copyright 2016 Davide Marnetto

    This file is part of Haplostrips.

    Haplostrips is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Haplostrips is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

'''

from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
import gzip
from os.path import isfile
import re
from pandas import *
from numpy import asarray
import platform

global nucs
nucs = ['A','T','C','G','.']

##opens vcf.gz file or normal .vcf file
#def vcf_safe_open(filename,filemode):
#	if re.search(r'\.vcf$', filename):
#		return open(filename,filemode)
#	elif re.search(r'\.vcf\.gz$', filename):
#		if isfile(filename+'.tbi'):
#			import pysam
#			return pysam.Tabixfile(filename)
#		else:
#			f = open(filename,filemode)
#			return gzip.GzipFile(fileobj=f)
#	else:
#		raise IOError(filename+': Unexpected file format (not .vcf nor .vcf.gz). option -h for usage')	

def safe_open(filename,filemode):
	f = open(filename,filemode)
	if re.search(r'\.gz$', filename):
		return gzip.GzipFile(fileobj=f)
	else:
		return f

def haps_from_ms(ms_infile,outhaps,ms_popfile):
	positions_found=False
	haps_df={}
	sample=0
	with safe_open(ms_infile, 'r') as ms:
		headerline=ms.readline()
		if re.match(r'^.*ms', headerline):
			header=headerline.rstrip('\r\n').split('-')
			pop=1
			pops=[]
			for i in header:
				if re.match(r'^I',i):
					pops_ms_opt=i.rstrip().split(' ')
					for n in pops_ms_opt[2:2+int(pops_ms_opt[1])]:
						pops=pops+[pop]*int(n)
						pop=pop+1
			if pop==1:
				pops=pops+[pop]*int(header[0].split(' ')[1])
			popfiledf=DataFrame([range(len(pops)),pops,["ms"]*len(pops)]).transpose()
			popfiledf.to_csv(ms_popfile,sep='\t',index=False,header=False)
		else: 
			raise IOError('Unexpected ms file format: first line is not the command line')
		for line in ms:
			if not (positions_found):
				if re.match(r'^positions', line):
					positions=line.rstrip(' \r\n').split(' ')[1:]
					haps_df['#CHROM']=["ms"]*len(positions)
					haps_df['POS']=range(len(positions))
					haps_cols=['#CHROM','POS']
					sample=0
					positions_found=True
				continue
			if re.match(r'^[01]+$', line):
				haps_df[str(sample)+"_1"]=list(line.rstrip('\r\n'))
				haps_cols.append(str(sample)+"_1")
				sample+=1
			else:
				break
		if sample==0:
			raise IOError('Unexpected ms file format: no samples')
		haps_df_out=DataFrame(haps_df, columns=haps_cols)
		haps_df_out.to_csv(outhaps,sep='\t',index=False)
		return None	
					

def haplorepl(matchobj):
	if not matchobj.group(2): return matchobj.group(1)+"\t."
	else: return matchobj.group(1)+"\t"+matchobj.group(2)


# deals with headers
def find_header(line,vcf_infile):
	if re.match(r'^##', line):
		return None	
	if re.match(r'^#', line):
		header= line.rstrip('\r\n').split('\t')
		if len(header) < 10 or header[0]!="#CHROM" or header[8]!="FORMAT":
			raise IOError(vcf_infile+': Unexpected file format (weird header)')
		hapids = []
		for c in header[9:len(header)] :
			hapids+=[c + '_1',c + '_2']
		header_haps = header[:9] + hapids
		return header_haps
	else:
		return 1


def extract_haps(vcf_infile,interval,GQ=None,MQ=None):
	#print "interval selected is chr "+str(interval[0])+" b "+str(interval[1])+" e "+str(interval[2])
	sites_found=0
	gsites_found=0
	header_haps=None
	int_haps=[]
	if not re.search(r'\.vcf(\.gz)?$', vcf_infile):
		raise IOError(vcf_infile+': Unexpected file format (not .vcf nor .vcf.gz). option -h for usage')
		
	if isfile(vcf_infile+'.tbi') and platform.system() != 'Windows':
		print 'using '+vcf_infile+'.tbi index file'
		try:
			import pysam
		except ImportError:
			raise ImportError(".tbi index found but no pysam module, needed if you want to use .tbi indexes")
		vcf = pysam.Tabixfile(vcf_infile)
		for line in vcf.header:
			header_haps=find_header(line,vcf_infile)
		if not header_haps:	
			raise IOError(vcf_infile+': Unexpected file format (input vcf has no header)')
		print "header found in file "+vcf_infile
		print "now look for coordinates: "+" ".join(str(x) for x in interval)
			
		for line in vcf.fetch(interval[0],interval[1],interval[2]):
			sites_found+=1
			newsite=site_parser(line,GQ,MQ,header_haps)
			if newsite:
				int_haps.append(newsite)
				gsites_found+=1

	else:
		vcf = safe_open(vcf_infile, 'r')
		for line in vcf:
			if not header_haps:
				header_haps=find_header(line,vcf_infile)
				if header_haps==1:	
					raise IOError(vcf_infile+': Unexpected file format (input vcf has no header)')
				if header_haps:
					print "header found in file "+vcf_infile
					print "now look for coordinates: "+" ".join(str(x) for x in interval)
				continue	
			#split up and treats the line as a site
			pos=re.match(r'([^\t]+)\t([0-9]+)', line)
			if pos.group(1)==interval[0] and int(pos.group(2))>interval[1]:
				if int(pos.group(2))<=interval[2]:
					sites_found+=1
					newsite=site_parser(line,GQ,MQ,header_haps)
					if newsite:
						int_haps.append(newsite)
						gsites_found+=1
				else:
					break


	vcf.close()
	print "sites found in "+vcf_infile+": "+str(sites_found)
	print "good sites found in "+vcf_infile+": "+str(gsites_found)
	for k in ['ID','QUAL','FILTER','FORMAT']:
		header_haps.remove(k)
	out_df=DataFrame(int_haps, columns=header_haps)
	out_df = out_df.astype({'POS': int})
        out_df_filt=(out_df != ".").any(axis=0)
        out_df_filt['INFO']=True
	return out_df.loc[:, out_df_filt]


# working on interval lines
def site_parser(line,GQ,MQ,header_haps):
	#line_haps=re.sub(r'([01.])[|/]([01.]):*[^\t]*',r'\1\t\2',line)
	line_details=line.rstrip('\r\n').split('\t',9)
	line_haps=re.sub(r'([01.])[|/]*([01.])*:*[^\t]*',haplorepl,line_details.pop()).split('\t')
	f = line_details+line_haps
	# take out systematic errors, indels and bad calls
	if re.search(r'SysErr', f[7]) or f[3] not in nucs or f[4] not in nucs:
		return None	
	f2 = line.rstrip('\r\n').split('\t')
	for i in range(9, len(f2)):
		d=dict(zip(f2[8].split(':'), f2[i].split(':')))
		if  re.match(r'\.[|/]\.', d['GT']):
			continue
		#print f[1],GQ,d['GQ'],MQ,d['MQ']
                for testq, keyq in zip([GQ,MQ],['GQ','MQ']):
			if keyq in d and testq != None:
				try:
					myq=float(d[keyq])
				except ValueError:
					myq=-1
				if myq < testq:
					#print f2[1]+': sample at column '+str(i)+' fails GQ'
					f[i*2-9] = '.'
					f[i*2-8] = '.'		
	#turn the line into a dictionary 
	int_line=dict(zip(header_haps, f))
	for k in ['ID','QUAL','FILTER','FORMAT']:
		del int_line[k]
	return int_line


# filters away samples with columnfilter in the sample name
#def filter_haps(haps_df,columnfilter):
#	newcols = haps_df.columns[:9].tolist()
#	newcols.extend([col for col in haps_df.columns[9:len(haps_df.columns)].tolist() if columnfilter in col])
#	return haps_df.loc[:,newcols]



# extract ancestral allele or gives none
def extract_AA(x):
	m = re.findall(r'AA=([actgACTG])',str(x))
	if len(set([x.upper() for x in m])) == 1:
		return m[0].upper()
	else:
		return None

# clean merged pandas dataframe from vcf
def clean_merged(df_raw,left_len,missing2ref):
        # give value to absent REF and ALT
        #print df_raw.to_csv("test.tmp1",sep='\t',index=False)
       	df_raw.loc[:,'REF_y'].fillna(df_raw.REF_x, inplace=True)
       	df_raw.loc[:,'REF_x'].fillna(df_raw.REF_y, inplace=True)
        #df_raw.loc[:,'ALT_x'].replace('.', None, inplace=True)
       	df_raw.loc[:,'ALT_y'].fillna(df_raw.ALT_x, inplace=True)
        df_raw.loc[:,'ALT_x'].fillna(df_raw.ALT_y, inplace=True)
       	dotted=df_raw.loc[((df_raw['ALT_x'] == '.') & (df_raw['ALT_y'] != '.')) | ((df_raw['ALT_y'] == '.') & (df_raw['ALT_x'] != '.')),['REF_x','ALT_x','REF_y','ALT_y']].as_matrix().tolist()
	if len(dotted)>0:
		for x in dotted:
			if x.index('.') == 1:
				x[1]= x[2] if x.count(x[2])==1 else x[3]
			else:
				x[3]= x[0] if x.count(x[0])==1 else x[1]

		df_raw.loc[((df_raw['ALT_x'] == '.') & (df_raw['ALT_y'] != '.')) | ((df_raw['ALT_y'] == '.') & (df_raw['ALT_x'] != '.')),['REF_x','ALT_x','REF_y','ALT_y']]=asarray(dotted)
	if missing2ref:
		df_raw.fillna('0', inplace=True)
	else:
		df_raw.fillna('.', inplace=True)
	#print df_raw.to_csv("test.tmp2",sep='\t',index=False)
	#switch inverted REF ALT
	switched = df_raw.ix[(df_raw['REF_x']==df_raw['ALT_y']) & (df_raw['ALT_x']==df_raw['REF_y']),left_len+3:].applymap(lambda x: 1-int(x) if x!="." else x)
	df_raw.ix[(df_raw['REF_x']==df_raw['ALT_y']) & (df_raw['ALT_x']==df_raw['REF_y']),left_len+3:] = switched
	switched_alleles = df_raw.ix[(df_raw['REF_x']==df_raw['ALT_y']) & (df_raw['ALT_x']==df_raw['REF_y']),['REF_x','ALT_x']].as_matrix()
	df_raw.ix[(df_raw['REF_x']==df_raw['ALT_y']) & (df_raw['ALT_x']==df_raw['REF_y']),['REF_y','ALT_y']] = switched_alleles
	#remove different REF ALT
	#df_clean = df_raw.query('(REF_y == REF_x) & (ALT_y == ALT_x)')
	df_clean = df_raw.loc[(df_raw['REF_y']==df_raw['REF_x']) & (df_raw['ALT_y']==df_raw['ALT_x'])].copy()
	#select columns
	df_clean.rename(columns={'REF_x':'REF','ALT_x':'ALT','INFO_x':'INFO'}, inplace=True)
	df_clean.loc[:,'INFO'] = df_clean.loc[:,'INFO']+";"+df_clean.loc[:,'INFO_y']
	df_clean.drop([col for col in df_clean.columns if re.search(r'_y$', col)],axis=1,inplace=True)
	return df_clean

# it s the function that does all the work
def haplotyper(	vcf_in,
		interval,
		outhaps,
		anc_allele,
                missing2ref,
		GQ,
		MQ,
		debug,
		):
	haps_df = []
	for vcf in vcf_in:
		haps_df.append([vcf[0],extract_haps(re.sub(r'#CHR',interval[0],vcf[1]),interval,GQ,MQ)])
	if all([len(thisdf[1].index) == 0 for thisdf in haps_df]):
		raise RuntimeError('no SNP found in any VCF at these coordinates. aborting.')
	merge_datasets=0
	while len(haps_df) > 1:
		merge_datasets=1
		# merge datasets
		if haps_df[0][0] == 0 and haps_df[1][0] == 0:
			haps_df_merged = [0,None]
			how_merge='inner'
		else:
			haps_df_merged = [1,None]
			if haps_df[0][0] == 1 and haps_df[1][0] == 1:
				how_merge='outer'
			elif haps_df[0][0] == 1:
				how_merge='left'
			else:
				how_merge='right'
		print "merging mode: "+how_merge
		df_raw = merge(haps_df[0][1],haps_df[1][1], on=['#CHROM','POS'],how=how_merge)
		haps_df_merged[1] = clean_merged(df_raw,len(haps_df[0][1].columns),missing2ref)
		#haps_df_merged[1]= df_raw
		haps_df.pop(0)
		haps_df[0] = haps_df_merged

	haps_df=haps_df[0][1]
	# There are duplicate positions where NEAN allele is ambiguous - delete
	#filter away double positions 
	haps_df.drop_duplicates('POS',keep=False,inplace=True)
	if merge_datasets:
		stderr.write(str(len(haps_df.index))+" sites after merging\n")
	
	# Switching any calls to make sure 0 = ancestral, 1 = derived
	if anc_allele:
	
		haps_df['AA'] = haps_df.loc[:,'INFO'].apply(extract_AA)
		haps_df.loc[(haps_df.REF != haps_df.AA) & (haps_df.ALT != haps_df.AA), 'AA'] = None
		haps_df.dropna(subset=['AA'],inplace=True)
		haps_df.ix[haps_df.AA == haps_df.ALT, 5:-1] = haps_df.ix[haps_df.AA == haps_df.ALT, 5:-1].applymap(lambda x: 1-int(x) if x!="." else x)
		haps_df.loc[haps_df.AA == haps_df.ALT, ['REF','ALT']] = haps_df.loc[haps_df.AA == haps_df.ALT, ['ALT','REF']].values
		haps_df.drop('AA', axis=1, inplace=True)
		print "sites after allele polarization for ancestral-derived: "+str(len(haps_df.index))
	
	haps_df.drop('INFO', axis=1, inplace=True)
	haps_df.sort_values('POS',inplace=True)
	haps_df.to_csv(outhaps,sep='\t',index=False)
	print "file created: "+outhaps

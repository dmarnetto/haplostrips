include haplostrips.config

install: default.poptable haplostrips.py haplotyper.py plotter.py plotter_general.r README.md LICENSE.txt dep.py haplostrips.config check_dep
	mkdir -p $(BIN_DIR)/../src/haplostrips
	cp -r $(wordlist 1,8,$^) makefile $(BIN_DIR)/../src/haplostrips
	sed 's|BIN_DIR=.*|BIN_DIR=../../bin|' $(word 9, $^) > $(BIN_DIR)/../src/haplostrips/haplostrips.config
	ln -is ../src/haplostrips/haplostrips.py $(BIN_DIR)/haplostrips

clean: haplostrips.config
	@echo "removing $(BIN_DIR)/haplostrips"
	@rm $(BIN_DIR)/haplostrips
	@if (echo $(BIN_DIR) | grep -qF '../../bin'); then \
	echo "you are in the source directory, if you want to remove it run: cd ..; rm -r haplostrips " ;\
	else \
		echo "removing $(BIN_DIR)/../src/haplostrips" ;\
		rm -r $(BIN_DIR)/../src/haplostrips ;\
	fi

check_dep:
	@echo "checking dependencies"
	@python ./dep.py

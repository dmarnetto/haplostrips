#from subprocess import call
import re

def confirm():
    """
    Ask user to enter Y or N (case-insensitive).
    :return: True if the answer is Y.
    :rtype: bool
    """
    answer = ""
    while answer not in ["y", "n"]:
        answer = raw_input("do you want to install/upgrade it [y/n]? ").lower()
    return answer == "y"



def mycmp(version1, version2):
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$','', v).split(".")]
    return cmp(normalize(version1), normalize(version2))

pandaneeded=False
pysamneeded=False

try:
	import pandas;
except ImportError:
	print "no module named pandas, but needed for haplostrips"
	pandaneeded=confirm()
else:
	if mycmp(pandas.__version__,"0.17.0") < 0:
		print "pandas is version "+pandas.__version__+", 0.17.0 or higher version needed for haplostrips"
		pandaneeded=confirm()
		if not pandaneeded:
			raise EnvironmentError("you will not be able to use haplostrips if you do not install/upgrade pandas")





try:
	import pysam;
except ImportError:
	print "no module named pysam, but needed if you want to use .tbi indexes"
	pysamneeded=confirm()
else:
	if mycmp(pysam.__version__,"0.7.5") < 0:
		print "pysam is version "+pysam.__version__+", 0.7.5 or higher version needed if you want to use .tbi indexes"
		pysamneeded=confirm()

#rscript="if ('gplots' %in% rownames(installed.packages())) {if(compareVersion(installed.packages()['gplots','Version'],'2.11.3')<0) {print(paste('gplots is version ',installed.packages()['gplots','Version'],', 2.11.3 or higher version needed, please see README'));}} else {print('R package gplots not found,  please see README')}"
#rscript_exec=["Rscript", "--vanilla", "-e", rscript]
#call(rscript_exec)


if pandaneeded or pysamneeded:
	try:
		import pip;
	except ImportError:
		print "no module named pip"
		print "pip is needed to install some dependencies, if you want to install it you can use your package manager.\nE.G. on Ubuntu, type:\nsudo apt-get install python-pip\n\nOn Red Hat:\nsudo yum install python-pip\n\nFor more info check https://pip.pypa.io/en/stable/installing/ ."
		raise EnvironmentError("dependencies not installed.")
	else:
		if pandaneeded:
			pip.main(["install", "--upgrade", "pandas"])
		if pysamneeded:
			pip.main(["install", "--upgrade", "pysam"])

